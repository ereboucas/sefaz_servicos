class Certificate

  def self.pkcs12(certificate_path, certificate_password)
    pfx_content = IO.binread(certificate_path)
    OpenSSL::PKCS12.new(pfx_content, certificate_password)
  end

end