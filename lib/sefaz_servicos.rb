require 'sefaz_servicos/version'
require 'sefaz_servicos/consulta/download'
require 'certificate'
require 'deathbycaptcha'
require 'deathbycaptcha_infosimples'
require 'sefaz_servicos/distribuicao/consulta'
require 'sefaz_servicos/distribuicao/manifestacao'
require 'xml_signature/signer'


module SefazServicos

  class Configuration
    attr_accessor :captcha_service
    attr_accessor :captcha_service_user
    attr_accessor :captcha_service_password
    def initialize
      @captcha_service = :infosimples
    end
  end

  class << self
    attr_writer :configuration
  end

  def self.configuration
    @configuration ||= Configuration.new
  end

  def self.reset
    @configuration = Configuration.new
  end

  def self.configure
    yield(configuration)
  end


end
