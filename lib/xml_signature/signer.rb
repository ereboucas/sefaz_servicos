module XmlSign
  class Signer
    attr_reader :certificate, :xml, :dsig_namespace, :tag

    def initialize(options)
      @xml              = options[:xml]
      @certificate      = options[:certificate]
      @tag              = options[:tag]
      @dsig_namespace   = { "ds"  => "http://www.w3.org/2000/09/xmldsig#" }
    end

    def sign!
      digest_value_portion.content     = calculate_digest_value
      signature_value_portion.content  = calculate_signature_value
      x509_certificate_portion.content = calculate_x509_certificate_value
      # xml_document.to_s

      canonicalize(xml_document)
    end

    private

    def xml_document
      @xml_document ||= Nokogiri::XML(xml, &:noblanks)
    end

    def signed_info_portion
      @signed_info_portion ||= xml_document.xpath(
        "//ds:SignedInfo", dsig_namespace
      ).first
    end

    def digest_value_portion
      @digest_value_portion ||= xml_document.xpath(
        "//ds:DigestValue", dsig_namespace
      ).first
    end

    def signature_value_portion
      @signature_value_portion ||= xml_document.xpath(
        "//ds:SignatureValue", dsig_namespace
      ).first
    end

    def x509_certificate_portion
      @x509_certificate_portion ||= xml_document.xpath(
        "//ds:X509Certificate", dsig_namespace
      ).first
    end

    def canonicalize(node)
      node.canonicalize(Nokogiri::XML::XML_C14N_1_0)
    end

    def calculate_digest_value
      # Create digest value element steps
      # gets the Resource to be signed
      # applies the transforms to that resource
      # applies the digest_method(SHA1) defined on algorithm attribute in trasnformed resource
      # after sha1 is applied to transformed resource, encode that with base64_encode64
      # removes spaces on begin and end of string with chomp method
      canonicalized_resource = canonicalize(xml_document.xpath(tag).first)

      transformed = OpenSSL::Digest::SHA1.digest(canonicalized_resource)
      Base64.encode64(transformed).strip
    end

    def sign_private_key(canonicalized_signature_info)
      certificate.key.sign(OpenSSL::Digest::SHA1.new, canonicalized_signature_info)
    end

    def calculate_signature_value
      # canonicalize the signed_info xml portion
      # sign private key with signature_method algorithm and canonicalize signed info xml portion
      # encodes it all
      canonicalized_signature_info = canonicalize(signed_info_portion)
      Base64.encode64(sign_private_key(canonicalized_signature_info)).gsub("\n", '')
    end

    def calculate_x509_certificate_value
      Base64.encode64(certificate.certificate.to_der).gsub(/\n/, "")
    end
  end
end
