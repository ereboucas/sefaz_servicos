require 'mechanize'

module SefazServicos
  module Consulta
    class Agent

      def initialize(certificate_path, certificate_password, is_nfe=true)
        @is_nfe = is_nfe
        @certificado = Certificate.pkcs12(certificate_path,certificate_password)
        @certificate_password = certificate_password
        @certificado_uuid = rand(1000)

        http1 = Net::HTTP::Persistent.new(@certificado_uuid)


        # Setup do Mechanize
        @mech ||= Mechanize.new
        @mech.shutdown
        @mech.agent.instance_variable_set('@http', http1)
        @mech.ssl_version = :TLSv1
        @mech.ca_file = 'lib/chains/AutoridadeCertificadoraRaizBrasileirav2.pem'
        @mech.cert = @certificado.certificate
        @mech.key = @certificado.key
  
      end
  
      def change_certificate(certificate, certificate_password)
        @certificado = certificate
        @certificate_password = certificate_password
        @mech.cert = certificate
        @mech.key = certificate_password
      end
  
      def connection
        @mech
      end
  
      def pagina_inicial
        @mech.get construct_url
      end
  
      def get_captcha
        image = @mech.page.images.last
        (image.node.attributes["src"].value.start_with?("data:image/png;base64")) ? get_image(image) : image.fetch.body
      end
  
      def captcha
        SefazServicos::Consulta::Captcha.new(self.get_captcha)
      end
  
      def download_button_text
        @is_nfe ? 'Download do documento*' : 'Download do documento'
      end
  
      private
  
      def construct_url
        url_production + uri
      end
  
      def url_production
        site = 'https://www.nfe.fazenda.gov.br'
        unless @is_nfe
          site = site.gsub('.nfe','.cte')
        end
        site
      end
  

      def uri
        site = '/portal/consulta.aspx?tipoConsulta=completa&tipoConteudo=XbSeqxE8pl8='
        unless @is_nfe
          site = site.gsub('XbSeqxE8pl8=','mCK/KoCqru0=')
        end
        site
      end

      def get_image(image)
        Base64.decode64(image.node.attributes['src'].value.split(',').last)
      end

    end
    
  end
end
