# -*- encoding : utf-8 -*-
require 'sefaz_servicos/consulta/agent'
require 'net/http/persistent'

module SefazServicos
  module Consulta
    class Download

      attr_accessor :certificate_id, :certificate_password

      def initialize(certificate_path, certificate_password)
        @sefaz_nfe = Agent.new(certificate_path,certificate_password,true)
        @sefaz_cte = Agent.new(certificate_path,certificate_password,false)

        @client = DeathByCaptcha.new(SefazServicos.configuration.captcha_service_user,
                                     SefazServicos.configuration.captcha_service_password,
                                     SefazServicos.configuration.captcha_service)
      end

      def change_certificate(certificate_id, certificate_password)
        @sefaz_nfe.change_certificate(certificate_id, certificate_password)
        @sefaz_cte.change_certificate(certificate_id, certificate_password)
      end

      def is_nfe?(chave)
        chave[20..21] == '55'
      end

      def sefaz_conforme_tipo(identificador)
        if is_nfe?(identificador)
          @sefaz_nfe
        else
          @sefaz_cte
        end
      end



      def download_xml(identificador)

        begin

          xml = nil
          error = nil

          #removo espacos e pontos
          identificador = identificador.gsub(' ','').gsub('.','')

          # carrego a pagina inicial de download (é preciso passar o identificador
          # para seber qual sefaz acessar
          sefaz = sefaz_conforme_tipo(identificador)
          pagina_inicial = sefaz.pagina_inicial
          pagina_nfe = nil

          # Quebra do Captcha
          captcha_nok = true

          captcha_holder = pagina_inicial.parser.css('#ctl00_ContentPlaceHolder1_imgCaptcha').first
          if captcha_holder.nil?
            captcha_holder = pagina_inicial.parser.css('#ContentPlaceHolder1_imgCaptcha').first
          end

          if captcha_holder

            image64 = captcha_holder.attr(:src).split(',', 2).last
            # client = DeathByCaptcha.new('taxweb', 'PkohpIY9RoDjd9QVnXhWwj9UqtB2CkWzEaXOFcV2', :infosimples)
            captcha = @client.nfe(raw64: image64)

            unless captcha.text.empty?
              captcha_text = captcha.text.downcase

              #Entra na página de visualização da sefaz
              form = pagina_inicial.forms.first
              form.texts[1].value = identificador
              form.texts[2].value = captcha_text
              # espero um pouco. a Sefaz trava consultas muito rápidas!
              sleep(4)

              button = form.button_with(:value => 'Continuar')
              sefaz.connection.submit(form, button)

              #Verifico se o captcha foi aceito
              pagina_nfe = sefaz.connection.page

              # Verifico se não tive erro de captcha
              captcha_nok = pagina_nfe.body.include?('igo da Imagem inv')
              if captcha_nok
                error = 'Consulta não disponibilizada, Tente Novamente'
                @client.report(captcha.id)
              end
            else
              error = 'Acesso não autorizado'
            end
          else
            error = 'Consulta não disponibilizada'
          end

          unless captcha_nok
            # Se existe um botão de download
            if pagina_nfe.body.include?(sefaz.download_button_text)
              form = pagina_nfe.forms.first
              button = form.button_with(:value => sefaz.download_button_text)
              if button
                xml_page = sefaz.connection.submit(form, button)
                if xml_page
                  span_err = xml_page.at('#ContentPlaceHolder1_lblResultadoConsulta')
                  span_err ||= xml_page.at('#ctl00_ContentPlaceHolder1_lblResultadoConsulta')
                  if span_err
                    error = span_err.text
                  end
                  xml = xml_page.body
                  if xml.nil? or xml.empty?
                    error = 'Xml não retornado, Tente Novamente'
                  end
                else
                  error = 'Não Retornou página de xml, Tente Novamente'
                end
              end
            else
              # Vejo se tem uma mensagem de erro
              error = Download.page_error_message(pagina_nfe)
            end
          end

        rescue Exception => e
          error = e.message
          xml = nil
        ensure
          ret = {
              error: error,
              xml: xml
          }
        end
        ret
      end
    end

  end

  def self.page_error_message(page)
    erro = 'Pagina não disponível'

    if page.body.include?('Consulta Resumida da NF-e Emitida em Conting')
      erro = 'Download não está disponível porque a nota foi emitida em contingência'
    end

    div_err = page.at('#ContentPlaceHolder1_lblResultadoConsulta')
    if div_err.present?
      erro = div_err.text
    end

    div_err = page.at('#ContentPlaceHolder1_bltMensagensErro')
    if div_err.present?
      erro = div_err.text
    end

    erro
  end

end
