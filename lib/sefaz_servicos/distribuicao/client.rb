# encoding: utf-8
require 'httpi'

module SefazServicos
  module Distribuicao
    class Client

      def initialize(url, soap_action, certificate_path, certificate_password)
        @url = url
        @soap_action = soap_action
        @certificado = Certificate.pkcs12(certificate_path,certificate_password)
        @certificate_password = certificate_password
      end

      def http_headers
        {
          'SOAPAction' => @soap_action,
          'Content-Type' => 'application/soap+xml; charset=utf-8'
        }
      end

      def request(data)
        request = HTTPI::Request.new(@url)
        ssl = HTTPI::Auth::SSL.new
        ssl.cert = @certificado.certificate
        ssl.cert_key = @certificado.key
        ssl.cert_key_password = @certificate_password
        ssl.ca_cert_file = 'lib/chains/AutoridadeCertificadoraRaizBrasileirav2.pem'
        ssl.verify_mode = :none
        ssl.ssl_version = :TLSv1

        request.headers = http_headers
        request.auth.instance_variable_set('@ssl', ssl)
        request.auth.type = :ssl
        request.body = data

        response = HTTPI.post(request, :net_http)
        response
      end

    end
  end
end
