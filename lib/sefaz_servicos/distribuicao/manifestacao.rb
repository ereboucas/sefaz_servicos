# -*- encoding : utf-8 -*-
require 'sefaz_servicos/distribuicao/client'
require 'zlib'
require 'stringio'
require 'active_support/all'

module SefazServicos
  module Distribuicao
    class Manifestacao

      def evento_nome(evento)
        @eventos[evento][0]
      end

      def evento_tp(evento)
        @eventos[evento][1]
      end

      def initialize(certificate, password, uf=nil)
        # monto a url conforme o meu estado
        url = 'https://www.nfe.fazenda.gov.br/RecepcaoEvento/RecepcaoEvento.asmx'
        action = 'http://www.portalfiscal.inf.br/nfe/wsdl/RecepcaoEvento/nfeRecepcaoEvento'
        @certificate = certificate
        @password = password
        @certificate_password = password
        @client = Client.new(url, action, certificate, password)
        @response = nil

        # Código do evento:
        # 210200 – Confirmação da Operação
        # 210210 – Ciência da Operação
        # 210220 – Desconhecimento da Operação
        # 210240 – Operação não Realizada
        @eventos = {
            confirmacao: ['Confirmacao da Operacao',210200],
            ciencia: ['Ciencia da Operacao',210210],
            desconhecimento: ['Desconhecimento da Operacao',210220],
            nao_realizada: ['Operacao nao Realizada',210240]
        }
      end

      def execute_manifestacao(cnpj, chave, evento, justificativa = nil)
        cnpj = cnpj.gsub('.','').gsub('-','').gsub('/','').gsub(' ','')
        request_xml = xml(cnpj, chave, evento, justificativa)
        request_xml = clean_xml(request_xml)
        @response = @client.request(request_xml)
        @response
      end

      def xml(cnpj, ch_nfe, evento, x_justificative = nil)
        dh_evento = Time.now.in_time_zone('Brasilia').strftime('%Y-%m-%dT%H:%M:%S%:z')
        desc_evento = evento_nome(evento)
        tp_evento = evento_tp(evento)
        c_orgao = '91'
        c_uf = '35'
        tp_amb = '1'
        n_seq_evento = '1'

        # Identificador da TAG a ser assinada, a3 regra de formação do Id é: “ID” + tpEvento + chave da NF-e + nSeqEvento
        inf_evento_id = "ID#{tp_evento}#{ch_nfe}0#{n_seq_evento}"

        xml = <<-XML
<?xml version="1.0" encoding="utf-8"?>
<envEvento versao="1.00" xmlns="http://www.portalfiscal.inf.br/nfe">
  <idLote>000000000000100</idLote>
  <evento versao="1.00" xmlns="http://www.portalfiscal.inf.br/nfe">
    <infEvento Id="#{inf_evento_id}">
      <cOrgao>#{c_orgao}</cOrgao>
      <tpAmb>#{tp_amb}</tpAmb>
      <CNPJ>#{cnpj}</CNPJ>
      <chNFe>#{ch_nfe}</chNFe>
      <dhEvento>#{dh_evento}</dhEvento>
      <tpEvento>#{tp_evento}</tpEvento>
      <nSeqEvento>#{n_seq_evento}</nSeqEvento>
      <verEvento>1.00</verEvento>
      <detEvento versao="1.00">
        <descEvento>#{desc_evento}</descEvento>#{xjust(x_justificative)}
      </detEvento>
    </infEvento>
    <Signature xmlns="http://www.w3.org/2000/09/xmldsig#">
      <SignedInfo>
        <CanonicalizationMethod
          Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315" />
        <SignatureMethod Algorithm="http://www.w3.org/2000/09/xmldsig#rsa-sha1" />
        <Reference URI="##{inf_evento_id}">
          <Transforms>
            <Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature" />
            <Transform Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315" />
          </Transforms>
          <DigestMethod Algorithm="http://www.w3.org/2000/09/xmldsig#sha1" />
          <DigestValue></DigestValue>
        </Reference>
      </SignedInfo>
      <SignatureValue></SignatureValue>
      <KeyInfo>
        <X509Data>
          <X509Certificate></X509Certificate>
        </X509Data>
      </KeyInfo>
    </Signature>
  </evento>
</envEvento>
        XML
        xml = clean_xml(xml)

        signed_xml = <<-XML
<?xml version="1.0" encoding="utf-8"?>
<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
<soap12:Header>
  <nfeCabecMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/RecepcaoEvento">
    <cUF>#{c_uf}</cUF>
    <versaoDados>1.00</versaoDados>
  </nfeCabecMsg>
</soap12:Header>
<soap12:Body>
  <nfeDadosMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/RecepcaoEvento">
    #{sign_xml(xml)}
  </nfeDadosMsg>
</soap12:Body>
</soap12:Envelope>
        XML

        signed_xml

      end

      def sign_xml(unsigned_xml)
        cert = Certificate.pkcs12(@certificate,@password)
        signer = XmlSign::Signer.new(xml:unsigned_xml, certificate: cert, tag: "//xmlns:infEvento")
        signed_xml =  signer.sign!
        remove_xml_encoding(signed_xml)
      end

      # verificar a real necessidade desse método
      def remove_xml_encoding(xml)
        xml.gsub(/^<\?xml([\w\W]+)\?\>/, '')
      end

      def clean_xml(xml)
        xml.gsub(/>\s+</, '><').gsub(/\n/, '')
      end

      # Tag xJust não pode ser informada vazia que retorna erro
      # maximo 255
      def xjust(justificativa = nil)
        return '' if justificativa.nil?
        "<xJust>#{justificativa.truncate(255)}</xJust>"
      end


    end
  end
end
