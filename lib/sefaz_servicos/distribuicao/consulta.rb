# -*- encoding : utf-8 -*-
require 'sefaz_servicos/distribuicao/client'
require 'zlib'
require 'stringio'
require 'active_support/all'

module SefazServicos
  module Distribuicao
    class Consulta

      def initialize(certificate,password)
        url = 'https://www1.nfe.fazenda.gov.br/NFeDistribuicaoDFe/NFeDistribuicaoDFe.asmx'
        action = 'http://www.portalfiscal.inf.br/nfe/wsdl/NFeDistribuicaoDFe/nfeDistDFeInteresse'
        @client = Client.new(url, action, certificate, password)
        @response = nil
      end

      def xml_method(cnpj, uf, amb,  nsu, consNSU = false)

        if consNSU
          method = "<consNSU><NSU>#{"%015d" % nsu.to_i}</NSU></consNSU>"
        else
          method = "<distNSU><ultNSU>#{"%015d" % nsu.to_i}</ultNSU></distNSU>"
        end
        <<-XML
<?xml version="1.0" encoding="UTF-8"?>
<env:Envelope xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:env="http://www.w3.org/2003/05/soap-envelope">
<env:Body>
<nfeDistDFeInteresse xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/NFeDistribuicaoDFe">
<nfeDadosMsg>
<distDFeInt xmlns="http://www.portalfiscal.inf.br/nfe" versao="1.00">
<tpAmb>#{amb}</tpAmb>
<cUFAutor>#{uf}</cUFAutor>
<CNPJ>#{cnpj}</CNPJ>
#{method}
</distDFeInt>
</nfeDadosMsg>
</nfeDistDFeInteresse>
</env:Body>
</env:Envelope>
        XML
      end

      def xml_dist(cnpj, uf, amb,  nsu = 0)
        xml_method(cnpj, uf, amb,  nsu, false)
      end

      def xml_cons(cnpj, uf, amb,  nsu = 0)
        xml_method(cnpj, uf, amb,  nsu, true)
      end

      def execute_dist(cnpj, nsu = 0)
        ambiente = 1
        uf = 35
        cnpj = cnpj.gsub('.','').gsub('-','').gsub('/','').gsub(' ','')
        @response = @client.request(xml_dist(cnpj, uf, ambiente, nsu))
        @response
      end

      def execute_cons(cnpj, nsu)
        ambiente = 1
        uf = 35
        cnpj = cnpj.gsub('.','').gsub('-','').gsub('/','').gsub(' ','')
        @response = @client.request(xml_cons(cnpj, uf, ambiente, nsu))
        @response
      end

      def returned_docs
        Consulta.extract_docs(@response.body)
      end

      def Consulta.response_info(response_xml)
        resp =  Nokogiri::XML(response_xml)
        resp.remove_namespaces!
        {
            cStat: resp.at_css('cStat').text,
            xMotivo: resp.css('xMotivo').text,
            dhResp: resp.css('dhResp').text.to_date,
            ultNSU: resp.css('ultNSU').text.to_i,
            maxNSU: resp.css('maxNSU').text.to_i
        }
      end

      def self.extract_docs(response_xml)
        results = { }
        body = Nokogiri::XML(response_xml)
        # Tiro os namespace para facilitar a busca
        body.remove_namespaces!
        body.xpath('//docZip').each do |docZip|
          nsu = docZip['NSU']
          # Notas vem zipadas dentro do response
          xml = ActiveSupport::Gzip.decompress(Base64.decode64(docZip.text))
          doc = Nokogiri::XML(xml)
          results[nsu] = {
            type: doc.root.name,
            xml:  xml
          }
        end
        results
      end

      def save_xmls(dir_path)
        returned_docs.each do |nsu, xml|
          File.open("#{dir_path}/#{nsu}.xml", 'w') do |f|
            f.write xml
          end
        end
      end

    end
  end
end
