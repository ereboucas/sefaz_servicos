module Soap

  class Client

    def initialize(wsdl_url, action, certificate, secret)
      @wsdl_url = wsdl_url
      @action = action
      @certificate = certificate
      @secret = secret
    end

    def http_headers
      {
        'SOAPAction' => %{"#{@action}"},
        'Content-Type' => 'application/soap+xml; charset=utf-8'
      }
    end

    def request(data)
      request = HTTPI::Request.new(@wsdl_url)
      ssl = HTTPI::Auth::SSL.new

      ssl.cert = @certificate
      ssl.cert_key = @secret
      ssl.cert_key_password = @secret

      # ssl.ca_cert_file = "/home/gkuhn1/Documentos/www.nfe.fazenda.gov.br"
      ssl.verify_mode = :none
      ssl.ssl_version = :TLSv1

      request.headers = http_headers
      request.auth.instance_variable_set("@ssl", ssl)
      request.auth.type = :ssl
      request.body = data

      HTTPI.post(request, :net_http)
    end

  end
end
