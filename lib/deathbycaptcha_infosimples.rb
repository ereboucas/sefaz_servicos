module DeathByCaptcha

  # Override Captcha model with a new #captcha= method.
  #
  class Captcha < DeathByCaptcha::Model
    def captcha=(value)
      @captcha = value.to_s
    end
  end

  # Override Client with a new .create method.
  #
  class Client
    def self.create(username, password, connection = :socket, options = {})
      case connection
      when :socket
        DeathByCaptcha::Client::Socket.new(username, password, options)
      when :http
        DeathByCaptcha::Client::HTTP.new(username, password, options)
      when :infosimples
        DeathByCaptcha::Client::Infosimples.new(username, password, options)
      else
        raise DeathByCaptcha::InvalidClientConnection
      end
    end
  end

  # HTTP client for Infosimples Captcha API.
  #
  class Client::Infosimples < Client

    BASE_URL = 'https://data.infosimples.com/api/v1/captcha/__action__.json'

    # Report incorrectly solved captcha for refund.
    #
    # @param [Integer] captcha_id Numeric ID of the captcha.
    #
    def report!(captcha_id)
      perform('report', :post, captcha: captcha_id)
      true
    end


    # Upload a captcha to DeathByCaptcha.
    #
    # This method will not return the solution. It's only useful if you want to
    # implement your own "decode" function.
    #
    # @return [DeathByCaptcha::Captcha] The captcha object (not solved yet).
    #
    def nfe(options)
      response = perform('nfe', :post, image64: load_captcha(options))
      return_captcha(response)
    rescue
      DeathByCaptcha::Captcha.new
    end

    private

    # Create a DeathByCaptcha::Captcha response from Infosimples.
    #
    def return_captcha(response)
      DeathByCaptcha::Captcha.new(
        captcha: response['receipt']['key'],
        text: response['data']['solution']
      )
    end

    # Perform an HTTP request to the DeathByCaptcha API.
    #
    # @param [String] action  API method name.
    # @param [Symbol] method  HTTP method (:get, :post, :post_multipart).
    # @param [Hash]   payload Data to be sent through the HTTP request.
    #
    # @return [Hash] Response from the DeathByCaptcha API.
    #
    def perform(action, method = :get, payload = {})
      payload.merge!(token: self.password)

      if method == :post
        uri = URI(BASE_URL.gsub('__action__', action))
        req = Net::HTTP::Post.new(uri.request_uri)
        boundary, body = prepare_multipart_data(payload)
        req.content_type = "multipart/form-data; boundary=#{boundary}"
        req.body = body

      else
        uri = URI("#{BASE_URL.gsub('__action__', action)}?#{URI.encode_www_form(payload)}")
        req = Net::HTTP::Get.new(uri.request_uri)
      end

      http = Net::HTTP.new(uri.host, uri.port)
      if (uri.scheme == "https")
        http.use_ssl = true
        http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      end
      res = http.request(req)

      JSON.parse(res.body)
    end

    # Prepare the multipart data to be sent via a :post_multipart request.
    #
    # @param [Hash] payload Data to be prepared via a multipart post.
    #
    # @return [String,String] Boundary and body for the multipart post.
    #
    def prepare_multipart_data(payload)
      boundary = "infosimples" + rand(1_000_000).to_s # a random unique string

      content = []
      payload.each do |param, value|
        content << '--' + boundary
        content << "Content-Disposition: form-data; name=\"#{param}\""
        content << ''
        content << value
      end
      content << '--' + boundary + '--'
      content << ''

      [boundary, content.join("\r\n")]
    end
  end
end
