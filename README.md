# SefazServicos

Welcome to your new gem! In this directory, you'll find the files you need to be able to package up your Ruby library into a gem. Put your Ruby code in the file `lib/sefaz_servicos`. To experiment with that code, run `bin/console` for an interactive prompt.

TODO: Delete this and the text above, and describe your gem

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'sefaz_servicos'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install sefaz_servicos


Crie um arquivo de inicializacao

config/initializers/sefaz_servicos.rb

```ruby
SefazServicos.configure do |config|
  config.captcha_service = :infosimples
  config.captcha_service_user = '...seu usuario...'
  config.captcha_service_password = '...sua senha...'
end
```
    
## Usage

API de Manifestacao de Notas

SefazServicos

### Consultar as notas pelo serviço de distribuição: SefazServicos::Distribuicao

  consulta( cnpj, certificado, nsu, inicio, fim )

  download_xml( cnpj, nsu, certificado )

  manifesta( cnpj, nsu, certificado, acao )

  
  
### Baixr o XML do PortalSefaz: SefazServicos::Consulta

  download_xml(identificador)
  
```ruby
identificador = '....chave 44 posicoes....'
certificado = 'path_to/certificado.pfx'
senha = '...sua senha inviolavel....'
downloader = SefazServicos::Consulta::Download.new(certificado, senha)
ret = downloader.download_xml(identificador)

unless ret[:error].present?
    xml = ret[:xml]
end
```            

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/sefaz_servicos.

