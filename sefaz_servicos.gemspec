# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'sefaz_servicos/version'


Gem::Specification.new do |spec|
  spec.name          = "sefaz_servicos"
  spec.version       = SefazServicos::VERSION
  spec.authors       = ["joselopesneto"]
  spec.email         = ["jose.lopes.santos.neto@gmail.com"]

  spec.summary       = %q{GEM para manifestação do destinatário e para download do XML da Sefaz}
  spec.description   = %q{Wrapper para api de manifestação do governo e de download de XML}
  spec.homepage      = "http://www.mcfox.com.br"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise 'RubyGems 2.0 or newer is required to protect against public gem pushes.'
  end

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.12'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_dependency 'twofish'
  spec.add_dependency 'mechanize'
  spec.add_dependency 'deathbycaptcha', '~> 5.0.0'
  spec.add_dependency 'httpi'
  spec.add_dependency 'savon'
  spec.add_dependency 'rubyntlm', '~> 0.3.2'
  spec.add_dependency 'activesupport'

end
