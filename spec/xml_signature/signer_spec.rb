require 'spec_helper'

describe XmlSign::Signer do
  describe '#sign!' do

    let(:certificate) do
      certificado = 'spec/files/certificado_teste_3.pfx'
      senha = '11023'
      Certificate.pkcs12(certificado,senha)
    end

    context 'when xml document has a tag to be signed' do
      context 'signs xml by tag Lote' do
        let(:xml) do
          File.read('spec/files/xml-examples/unsigned/tag_lote.xml')
        end

        subject do
          described_class.new(
            xml: xml, certificate: certificate, tag: '//Lote'
          )
        end

        it do
          expect(subject.sign!).to eq(
            File.read(
              './spec/files/xml-examples/signed/tag_lote.xml'
            )
          )
        end
      end

      context 'signs xml by tag infNFe' do
        let(:xml) do
          File.read('./spec/files/xml-examples/unsigned/tag_infNFe.xml')
        end

        subject do
          described_class.new(
            xml: xml, certificate: certificate, tag: './/xmlns:infNFe'
          )
        end

        it do
          expect(subject.sign!).to eq(
            File.read('./spec/files/xml-examples/signed/tag_infNFe.xml')
          )
        end
      end
    end

    context 'when xml document has to be completely signed'
  end
end