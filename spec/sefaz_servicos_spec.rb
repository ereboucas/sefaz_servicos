require 'spec_helper'

describe SefazServicos do

  before do
    SefazServicos.configure do |config|
      config.captcha_service = :infosimples
      config.captcha_service_user = 'taxweb'
      config.captcha_service_password = 'PkohpIY9RoDjd9QVnXhWwj9UqtB2CkWzEaXOFcV2'
    end
  end

  it 'has a version number' do
    expect(SefazServicos::VERSION).not_to be nil
  end

  it 'deve recuperar um xml dado a chave, certificado e senha corretos' do
    chave = '35 1607 04206050001071 55 010 001603809 1 93895246 2'
    certificado = 'spec/files/certificado_teste_3.pfx'
    senha = '11023'
    downloader = SefazServicos::Consulta::Download.new(certificado, senha)
    ret = downloader.download_xml(chave)
    expect(ret[:xml]).not_to eq(nil)
    expect(ret[:xml]).to start_with '<nfeProc versao'
  end

  it 'deve dar um erro legivel de tento baixar um xml dado a chave, certificado e senha incorretos' do
    chave = '35 1607 04206050001071 55 010 001603809 1 93895246 2'
    certificado = 'spec/files/certificado_teste_1.pfx'
    senha = '7835526386'
    downloader = SefazServicos::Consulta::Download.new(certificado, senha)
    ret = downloader.download_xml(chave)
    expect(ret[:error]).not_to eq(nil)
    expect(ret[:error]).to match(/CNPJ/)
  end

  it 'deve consultar a lista de distribuicao para o cnpj informado' do
    cnpj = '11.023.029/0001-05'
    certificado = 'spec/files/certificado_teste_3.pfx'
    senha = '11023'
    distribuicao_consulta = SefazServicos::Distribuicao::Consulta.new(certificado,senha)
    response = distribuicao_consulta.execute_dist(cnpj, 20)
    expect(response.class).to eq(HTTPI::Response)
    expect(response.body).not_to match(/<soap:Fault>/)
  end

  it 'deve tratar a resposta da distribuicao' do
    xml = File.read('spec/files/distribuicao_response.xml')
    result = SefazServicos::Distribuicao::Consulta.extract_docs(xml)
    expect(result.keys.count).to eq(22)
    info = SefazServicos::Distribuicao::Consulta.response_info(xml)
    expect(info[:ultNSU]).to eq(37)
    expect(info[:maxNSU]).to eq(37)
  end

  it 'deve retornar a nota a partir de um NSU' do
    cnpj = '11.023.029/0001-05'
    certificado = 'spec/files/certificado_teste_3.pfx'
    senha = '11023'
    distribuicao_consulta = SefazServicos::Distribuicao::Consulta.new(certificado,senha)

    # resNfe
    nsu = 16
    response = distribuicao_consulta.execute_cons(cnpj,nsu)
    expect(response.body).not_to match(/<soap:Fault>/)

    # nfeProc
    nsu = 18
    response = distribuicao_consulta.execute_cons(cnpj,nsu)
    expect(response.body).not_to match(/<soap:Fault>/)

    # resEvento
    nsu = 20
    response = distribuicao_consulta.execute_cons(cnpj,nsu)
    expect(response.body).not_to match(/<soap:Fault>/)
  end

  it 'deve executar a ciencia' do
    cnpj = '11.023.029/0001-05'
    certificado = 'spec/files/certificado_teste_3.pfx'
    senha = '11023'
    manifestacao = SefazServicos::Distribuicao::Manifestacao.new(certificado,senha)
    chave = '41160405330384000124550010007793521608291139'
    response = manifestacao.execute_manifestacao(cnpj,chave,:ciencia)
    expect(response.body).not_to match(/<soap:Fault>/)
    expect(response.body).to match(/Rejeicao: Duplicidade de evento/)
  end

end
